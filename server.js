// 1st, require all packages needed
 const express = require("express");
 const mongoose = require("mongoose");
 const cors = require("cors");
	

// 2nd, server/app creation
 // create port
  const port = 4000;

 //contain express to a variable app
  const app = express();

 //middlewares
  app.use(express.json());
  app.use(express.urlencoded({extended:true}));
 
  app.use(cors());


 //routes (server requires routes)
  const userRoute = require("./03.Routes/userRoute.js")
  const productRoute = require("./03.Routes/productRoute.js")
  const orderRoute = require("./03.Routes/orderRoute.js")

 //routing
  //localhost:6000/user/
   app.use("/user", userRoute);
  //localhost:6000/product/
   app.use("/product", productRoute);
  //localhost:6000/order/
   app.use("/order", orderRoute);


// 3rd, establish database connection

 // to disable DeprecationWarning from Mongoose
	mongoose.set('strictQuery', true);

 // mongoDBconnection
	mongoose.connect("mongodb+srv://admin:admin@batch245-sanroque.9babxkw.mongodb.net/capstone-2?retryWrites=true&w=majority", {
			useNewUrlParser: true,
			useUnifiedTopology: true
		})

 // check database connection
	let db = mongoose.connection
		
	// for error handling
	 db.on("error", console.error.bind(console, "Connection Error!"))
		
	// validation of connection
	 db.once("open", ()=> console.log("We are now connected to the cloud!!"));


// will console, if server is running successfully
 app.listen(port, () => console.log(`Server is running at port ${port}!!`))