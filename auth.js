// require jwt
 const jwt = require("jsonwebtoken");

// Create Secret Code
 const secret = "ElectriciTea";

// Create Access Token
 module.exports.createAccessToken = (user) => {
 	const data = {
 		_id: user._id,
 		email: user.email,
 		isAdmin: user.isAdmin
	 }

	return jwt.sign(data, secret, {});
 };

// Token Verification
 module.exports.verify = (request, response, next) => {
 	let token = request.headers.authorization;

 	// if token exist, should not be undefined
 	if(typeof token !== "undefined"){
 		// remove "Bearer" prefix using splice
 		token = token.slice(7, token.length);
		
		return jwt.verify(token, secret, (err,data) => {
			if(err){
				return response.send({auth: "Failed"});
			} else{
				next()
			}
		 })
 	 } else{ //if token does not exist
 	 	return response.send({auth: "Failed!"})
 	 }
 };

// Token Decryption
 module.exports.decode = (token) => {
 	if(typeof token !== "undefined"){
 		token = token.slice(7, token.length);

 		return jwt.verify(token, secret, (err,data) => {
 			if(err){
 				return null;
 			} else{
 				return jwt.decode(token, {complete:true}).payload;
 			}
 		})
 	 } else{ //if token does not exist
 	 	return null;
 	 }
 };