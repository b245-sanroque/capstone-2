/*
GITBASH PROPER
 - git init repository
 - install npm packages
	* express 
	* mongoose
	* cors
	* bcrypt
	* jsonwebtoken
 - init npm (/init npm -y)
	- to create package-lock.json
 - add .gitignore
	- to ignore files (like node_modules)


Server/App > Routes > Controllers > Models
	- server > requires routes
	- route > requires controllers
	- controller > requires model


SERVER CREATION
 1st, require all packages needed
 	- express
 	- mongoose
 	- cors
 2nd, server/app creation
 	- create port
 	- contain express to a variable app
 	- create middlewares
 	- check if server is running
 3rd, establish database connection
 	- establish mongoDBconnection
 	- check database
 4th, routes and routing
 	- require routes needed
 	- create routing


MODELS CREATION
 ** Users, Products and Orders

 require all packages needed
 	- mongoose
 create Schema
 	- use mongoose to create user, product and order schemas
 make model exportable
 	- use and contain to "module.exports"


CONTROLLERS CREATION
 ** userCon, prodCon, orderCon

 require all packages needed
 	- mongoose?
 	- bcrypt
 	- jsonwebtoken?
 	- auth?	
  separation of concern 
  	- require models needed
  create controllers 
  	- create functions needed
  make model exportable
 	- use "module.exports"


ROUTES CREATION

 require all packages needed
 	- express
 create router
 	- use express.Router()
 separation of concern
 	- require controllers
    - require function routes
 make model exportable
 	- use and contain to "module.exports"


AUTHENTICATION

 create auth.js
 require package needed
 	- jsonwebtoken
 create secret code
 token creation
 	- create access token
 token verification
 	- create verfify function
 token decryption
 	- create decode function


*/
