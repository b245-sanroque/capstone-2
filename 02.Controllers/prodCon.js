// require all packages/models needed 
 const Product = require("../01.Models/productSchema.js");
 const auth = require("../auth.js");

 
/* - C O N T R O L L E R S - */

//*refactored*
// GetAllProducts Controller *ADMIN ONLY
 module.exports.getAllProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
        return response.send(false);//`You don't have access to do this action!`
    } else{
        Product.find({})
        .then(result => response.send(result))
        .catch(error => response.send(false))
    }
 };

//*refactored*
// Create Product *ADMIN ONLY
 module.exports.addProduct = (request,response) => {
  const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
      return response.send(false); //`You don't have access to do this action!`
    } else{
      let input = request.body;

      Product.findOne( { name: input.name } )
      .then(result => {
        if(result !== null){ 
          return response.send(false); //`Product already exist! Add another one.`
        } else{
          let newProduct = new Product({
            name: input.name,
            description: input.description,
            price: input.price,
            category: input.category
          });

          return newProduct.save()
          .then(product => response.send(product)) //`${input.name} has been added to Products List!`
          .catch(error => response.send(false))
        }
      }).catch(error => response.send(false))
    }
  };

//*refactored*
// Retrieve all Active Product
 module.exports.activeProducts = (request, response) => {
 	Product.find({isActive: true})
 	.then(result => response.send(result)) //displays active
 	.catch(error => response.send(false));
 }

//*refactored*
// Retrieve all Inactive Product *ADMIN ONLY
  module.exports.inactiveProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
        return response.send(false);//`You don't have access to do this action!`
    } else{
        Product.find({isActive: false})
        .then(result => response.send(result)) //displays inactive
        .catch(error => response.send(false))
    }
 };

//*refactored*
// Retrieve Single Product
  module.exports.productDetails = (request, response) => {
 	const productId = request.params.productId;
  // console.log(productId)

 	Product.findById(productId)
 	.then(result => response.send(result)) //displays product
 	.catch(error => response.send(false))
 }

//*refactored*
// Update Product Info *ADMIN ONLY
 module.exports.updateProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;
  const input = request.body;

  if(!userData.isAdmin){
  		return response.send(false); //`You are not authorized to do this action!`
  } else{
  	Product.findById(productId)
  	.then(result => {
  	 if(result === null){
  	 	return response.send(false); //"Product ID provided is invalid!"
  	 } else{
  	 	let updatedProduct = {
  	 		name: input.name,
  	 		description: input.description,
  	 		price: input.price,
        category: input.category
  	 	 } 
  	 	Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
  	 	.then(result => response.send(result)) //`${result.name} has been updated.`
  	 	.catch(error => response.send(false))
  	 }
  	}).catch(error => response.send(false));
  }
 };

//*refactored*
// Archive Product *ADMIN ONLY
 module.exports.archiveProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;
  const input = request.body;

  if(!userData.isAdmin){
  	return response.send(false); //`You are not authorized to do this action!`
  } else{
  	 Product.findById(productId)
  	 .then(result => {
  		if(result === null){
  			return response.send(false); //"Product ID provided is invalid!"
  		} else{
        // console.log(result.isActive)
        // console.log(!result.isActive)
  			let archivedProduct = { isActive: !result.isActive }

  			Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
  			.then(result => {
            // console.log(result.isActive)
            if(!result.isActive){
                // console.log(result.isActive)
                return response.send(result); //`${result.name} has been successfully archived!`
            } else{
                // console.log(result.isActive)
                return response.send(result); //`${result.name} has been successfully unarchived!`
            }            
        }).catch(error => response.send(false))
  		}
  	 }).catch(error => response.send(false));
  }
 };


// Retrieve all milktea Product
  module.exports.milktea = (request, response) => {
    Product.find({isActive: true, category: "MilkTea"})
    .then(result => response.send(result)) //displays milktea
    .catch(error => response.send(false));
  }
// Retrieve all chix Product
  module.exports.chix = (request, response) => {
    Product.find({isActive: true, category: "ChixND"})
    .then(result => response.send(result)) //displayschix
    .catch(error => response.send(false));
  }
// Retrieve all manoys Product
  module.exports.manoys = (request, response) => {
    Product.find({isActive: true, category: "Manoys"})
    .then(result => response.send(result)) //manoys
    .catch(error => response.send(false));
  }
