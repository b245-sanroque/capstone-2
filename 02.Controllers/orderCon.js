// require packages/models needed
 const mongoose = require("mongoose");
 const auth = require("../auth.js");

 const Order = require("../01.Models/orderSchema.js");
 const Product = require("../01.Models/productSchema.js");
 // const User = require("../01.Models/userSchema.js");
 

/* - C O N T R O L L E R S - */

// Order List
 	// if User - access only to their orders
	// if admin - access to all orders
 module.exports.orders = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  	// console.log(userData);

  if(userData.isAdmin){
  		Order.find({})
  		.then(result => response.send(result))
  		.catch(error => response.send(false))
  } else{
  		Order.find({userId: userData._id})
  		.then(result => response.send(result))
  		.catch(error => response.send(false))
  }
 };

// Create Order (checkout) *NON-ADMIN ONLY
 module.exports.createOrder = (request,response) => {
  const userData = auth.decode(request.headers.authorization);
  // console.log(userData);
  const productId = request.params.productId;

  if(userData.isAdmin){
 	 return response.send(false);//`You don't have access to do this action!`
  } else{
  	 let input = request.body;

  	 Product.findById(productId)
     .then(result => {
 	 	if(result === null){
 	 		return response.send(false);//`Sorry! ${input.name} is unavailable!`
 	 	} else{
 	 		// console.log(result);
 	 		let newOrder = new Order({
 			 userId: userData._id,
 			 productId: result.id,
 			 productName: result.name,
 			 quantity: 1,
 			 totalAmount: 1*result.price
 			});
 			// console.log(newOrder);
 			// return response.send(`Order Created`);
 			return newOrder.save()
 			.then(order => {
 				return response.send(order); //`Order has been successfully created!`
 			}).catch(error => response.send(false));
 	 	}
     }).catch(error => response.send(false));
  }
 };

// Update Quantity *LOGGED-IN USER
 module.exports.updateOrder = (request, response) => {
   const userData = auth.decode(request.headers.authorization);
   const input = request.body;
   // console.log(userData);

   Order.findOne({userId: userData._id})
   .then(result => {
      let productId = result.productId;
      let price = result.totalAmount/result.quantity;
      // console.log(result)

      Order.findOne({productId: productId})
      .then(result =>{
         if(result.productName !== input.name){
            return response.send(false);//`Invalid! Please create order first!`
         } else{
            // console.log(result.productName)
            // console.log(input.name)
            // console.log(result)
            let newQuan = {
               quantity: input.quantity,
               totalAmount: input.quantity*price
            }
            // console.log(result)
            // console.log(newQuan)
            // response.send("wow")
            Order.findByIdAndUpdate(result._id, newQuan, {new: true})
            .then(result => response.send(result))//`${input.name}'s Quantity has been updated.`
            .catch(error => response.send(false))
         }
      }).catch(error => response.send(false));
   }).catch(error => response.send(false));
 };

