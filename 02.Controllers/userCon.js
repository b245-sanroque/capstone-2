// require all packages/models needed 
 const mongoose = require("mongoose");
 const bcrypt = require("bcrypt");

 const User = require("../01.Models/userSchema.js");
 const auth = require("../auth.js");


/* - C O N T R O L L E R S - */

//*refactored*
// GetAllUsers Controller *ADMIN ONLY
 module.exports.getAllUsers = (request, response) => {
 const userData = auth.decode(request.headers.authorization);

 	if(!userData.isAdmin){
 		return response.send(false); //You don't have access to do this action!
 	} else{
 		User.find({})
 		.then(result => response.send(result))
        .catch(error => response.send(error))
 	 }
 };

//*refactored*
// User Registration Controller (register)
 module.exports.userRegistration = (request, response) => {
 	const input = request.body;

    // check if username is already taken
    User.findOne( { username: input.username } )
    .then(result => {
     if(result !== null){
        return response.send(false); //Username already taken! Please provide another one.
     } else{
        // check email if existing
        User.findOne( { email: input.email } )
        .then(result => {
         if(result !== null){
            return response.send(false); //Email provided already exist!
         } else{
            let newUser = new User({
                username: input.username,
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                password: bcrypt.hashSync(input.password, 10),
                mobileNo: input.mobileNo
            });

            newUser.save()
            .then(save => response.send(save)) //New User successfully added!
            .catch(error => response.send(false)); //error
         }
        }).catch(error =>response.send(false)); //error        
     }
    }).catch(error =>response.send(false)); //error
 }

//*refactored*
// User Aunthentication Controller (login)
 module.exports.userAuthentication = (request, response) => {
 	const input = request.body

 	User.findOne( { username: input.username } )
 	.then(result =>{
 		//if username is registered
 		if(result === null){
            // console.log(result);
 			return response.send(false); //username not found. Please register before logging in.
 		} else{
 			//check if password match
            // console.log(result);
 			const isPasswordCorrect = bcrypt.compareSync( input.password, result.password );

 			if(isPasswordCorrect){
 				return response.send( {auth: auth.createAccessToken(result)} );
 			} else{
 				return response.send(false); //Incorrect Password!
 			 }
 		 }
 	}).catch(error => {
 		return response.send(false); //error
 	 })
 };

// Get User Details *LOGGED-IN USER ONLY
 module.exports.userProfile = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    // console.log(userData);

    User.findOne({email: userData.email})
    .then(result => {
        result.password = "";
        // console.log(result);
        return response.send(result); //
    }).catch(error => response.send(error)); //
 };

//*refactored*
// Update User Details **
    // User - can change own details, cannot make oneself and others an admin
    // Admin - can change own details, can also change all details of users
 module.exports.userUpdate = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const input = request.body;
  const userId = request.params.userId;
  // console.log(userId)
  
  User.findOne({email: userData.email})
  .then(result => {
     // userId not provided
     if(userId === undefined){ 
        if(input.isAdmin){
            // console.log(input.isAdmin);
            return response.send(false); //You cannot make yourself an admin.
        } else{
            // console.log(userData.isAdmin);
            // console.log(input.isAdmin);

            // check if username is already taken
            User.findOne( { username: input.username } )
            .then(result => {
             if(result !== null){
                return response.send(false); //Username already taken! Please provide another one.
             } else{
                let updatedUser = {
                    username: input.username,
                    firstName: input.firstName,
                    lastName: input.lastName,
                    email: input.email,
                    mobileNo: input.mobileNo
                } 
                // console.log(updatedUser)
                // return response.send(`userId not provided`)

                User.findByIdAndUpdate(userData._id, updatedUser, {new: true})
                .then(result => response.send(result)) //Your Details has been updated.
                .catch(error => response.send(false)) //error
             }
            }).catch(error =>response.send(error)) //error
        }  
     } else{        
        if(!userData.isAdmin){ // admin time
            return response.send(false); //You are not authorized to change someone's user details!
        } else{
            User.findById(userId)
            .then(result => {
                if(result === null){
                    return response.send(false); //User ID provided is invalid!
                } else{
                    let updatedUser = {
                        username: input.username,
                        email: input.email,
                        isAdmin: input.isAdmin
                     } 
                    // console.log(updatedUser)
                    User.findByIdAndUpdate(userId, updatedUser, {new: true})
                    .then(result => response.send(true)) //`${result.username}'s details has been updated.`
                    .catch(error => response.send(false)) //error
                 }
            }).catch(error => response.send(false)); //error          
            // return response.send(`userId provided`)
        }
     }
  }).catch(error => response.send(false)); //error
 };

//*refactored
// Change User Password **
    // User - can change own password
    // Admin - can anyone's password
 module.exports.pwChange = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const input = request.body;
  const userId = request.params.userId;
  // console.log(userId)

  User.findOne({email: userData.email})
  .then(result => {
    if(userId === undefined){ // userId not provided (not Admin)
        const isPasswordCorrect = bcrypt.compareSync(input.currentPassword, result.password);
        if(!isPasswordCorrect){
            return response.send(false); //Current Password incorrect!
        } else{
            let updatedUser = {
                password: bcrypt.hashSync(input.newPassword, 10)
            }
           // console.log(updatedUser) 
           // console.log(isPasswordCorrect)
           User.findByIdAndUpdate(userData._id, updatedUser, {new: true})
           .then(result => response.send(true)) //Your Password has been changed.
           .catch(error => response.send(false))
         }
        // return response.send(`userId not provided`)
    } else{ 
       if(!userData.isAdmin){ // admin time
          return response.send(false); //You don't have access to change someone's password!
       } else{
           User.findById(userId)
           .then(result => {
                let updatedUser = {
                    password: bcrypt.hashSync(result.username, 10)
                }
                // console.log(updatedUser) 
                User.findByIdAndUpdate(userId, updatedUser, {new: true})
                .then(result => response.send(result)) //`${result.email}'s Password has been reset.`
                .catch(error => response.send(false)) //error
                // return response.send(`yo`)

           }).catch(error => response.send(false)); //error
       }
       // return response.send(`userId provided`)
    }
  }).catch(error => response.send(false)); //error

 };    



// setAdmin
 module.exports.setAdmin = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const userId = request.params.userId;
  const input = request.body;

  if(!userData.isAdmin){
    return response.send(false); //`You are not authorized to do this action!`
  } else{
     User.findById(userId)
     .then(result => {
        if(result === null){
            return response.send(false); //"Product ID provided is invalid!"
        } else{
            let adminSet = { isAdmin: !result.isAdmin }

            User.findByIdAndUpdate(userId, adminSet, {new: true})
            .then(result => {
            // console.log(result.isAdmin)
            if(!result.isAdmin){
                // console.log(result.isAdmin)
                return response.send(result); //`${result.name} has been successfully archived!`
            } else{
                // console.log(result.isAdmin)
                return response.send(result); //`${result.name} has been successfully unarchived!`
            }            
        }).catch(error => response.send(false))
        }
     }).catch(error => response.send(false));
  }
 };

// Retrieve Single Product
   module.exports.userDetails = (request, response) => {
    const userId = request.params.userId;
   // console.log(productId)

    User.findById(userId)
    .then(result => response.send(result)) //displays product
    .catch(error => response.send(false))
  }