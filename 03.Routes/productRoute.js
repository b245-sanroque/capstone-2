// require all packages/controllers needed
 const express = require("express");
 const router = express.Router();

 const prodCon = require("../02.Controllers/prodCon.js");
 const auth = require("../auth.js");


/* - R O U T E S - */

// Route to get AllProducts (get) *ADMIN ONLY
 // router.get("/", prodCon.getAllProducts);
 router.get("/", auth.verify, prodCon.getAllProducts);
 //localhost:6000/product

// Create Product (post) *ADMIN ONLY
 router.post("/createProduct", auth.verify, prodCon.addProduct);
 //localhost:6000/product/createProduct

// Retrieve all Active Product (get)
 router.get("/available", prodCon.activeProducts);
 //localhost:6000/product/available

 // Retrieve all Inactive Product (get) *ADMIN ONLY
 router.get("/notAvail", auth.verify, prodCon.inactiveProducts);
 //localhost:6000/product/notAvail


// Retrieve all milktea Product (get)
 router.get("/milktea", prodCon.milktea);
 //localhost:6000/product/milktea

// Retrieve all chixND Product (get) 
 router.get("/chix", prodCon.chix);
 //localhost:6000/product/chix

// Retrieve all manoys Product (get)
 router.get("/manoys", prodCon.manoys);
 //localhost:6000/product/manoys


// Retrieve Single Product (get)
 router.get("/:productId", prodCon.productDetails);
 //localhost:6000/product/:productId

// Update Product Info (put) *ADMIN ONLY
 router.put("/update/:productId", auth.verify, prodCon.updateProduct)
 //localhost:6000/product/update/:productId

// Archive Product (put) *ADMIN ONLY
 router.put("/archive/:productId", auth.verify, prodCon.archiveProduct)
 //localhost:6000/product/archive/:productId

// make router exportable
 module.exports = router;