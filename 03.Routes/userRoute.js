// require all packages/controllers needed
 const express = require("express");
 const router = express.Router();

 const userCon = require("../02.Controllers/userCon.js");
 const auth = require("../auth.js");


/* - R O U T E S - */

// Route to get AllUsers (get) *ADMIN ONLY
 // router.get("/", userCon.getAllUsers);
 router.get("/", auth.verify, userCon.getAllUsers);
 //localhost:6000/user

// Route for userRegistration (post)
 router.post("/register", userCon.userRegistration);
 //localhost:6000/user/register

// Route for User Authentication (post)
 router.post("/login", userCon.userAuthentication);
 //localhost:6000/user/login

// Route for User Details (get) *LOGGED-IN USER ONLY
 router.get("/profile", auth.verify, userCon.userProfile);
 //localhost:6000/user/details

// Update User Details (put) *except password huhu
    // User - can change own details, cannot make oneself and others an admin
    // Admin - can change own details, can also change all details of users
 router.put("/update", auth.verify, userCon.userUpdate);
 router.put("/update/:userId", auth.verify, userCon.userUpdate);
 //localhost:6000/user/update

// Change User Password (put)
    // User - can change own password
    // Admin - can anyone's password
 router.put("/pwChange", auth.verify, userCon.pwChange);
 router.put("/pwChange/:userId", auth.verify, userCon.pwChange);
 //localhost:6000/user/update

 // setAdmin (put) *ADMIN ONLY
  router.put("/setAdmin/:userId", auth.verify, userCon.setAdmin)

// get single user// Retrieve Single Product (get)
 router.get("/:userId", userCon.userDetails);
 //localhost:6000/user/:userId

// make router exportable
 module.exports = router;