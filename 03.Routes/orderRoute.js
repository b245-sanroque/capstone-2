const express = require("express");
const router = express.Router();

const orderCon = require("../02.Controllers/orderCon.js");
const auth = require("../auth.js");


/* - R O U T E S - */

//Route for Order List (get) 
	// if User - access only to their orders
	// if admin - access to all orders
 router.get("/", auth.verify, orderCon.orders);
 //localhost:6000/order

//Route for Create Order (post) *NON-ADMIN ONLY
 router.post("/checkout/:productId", auth.verify, orderCon.createOrder);
 //localhost:6000/order/checkout


// Update Quantity (put) *LOGGED-IN USER
 router.put("/update", auth.verify, orderCon.updateOrder);
 //localhost:6000/order/update


// make router exportable
module.exports = router;