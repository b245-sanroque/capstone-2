const mongoose = require("mongoose");

// Create Product Schema
 const productSchema = new mongoose.Schema({
 	name: {
 		type: String,
 		required: [true, "Name is required!"]
 	},
 	description: {
 		type: String,
 		required: [true, "Description is required!"]
 	},
 	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
 	category: {
		type: String,
		required: [true, "Category is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	creationDate: {
		type: Date,
		default: new Date()
	}
 })

module.exports = mongoose.model("Product", productSchema);