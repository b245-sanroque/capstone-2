const mongoose = require("mongoose");

// Create Order Schema
 const orderSchema = new mongoose.Schema({
  userId: {
  	type: String,
  	required: [true, "User ID is required!"]
  },
  productId: {
      type: String,
      required: [true, "Product ID is required!"]
    },
  productName:{
	  type: String,
	  required: [true, "Product ID is required!"]
	},
	quantity: {
	  type: Number,
	  required: [true, "Quantity is required!"]
  	},
  totalAmount: {
  	type: Number,
  	required: [true, "Total Amount is required!"]
  },
  purchasedOn: {
  	type: Date,
	default: new Date()
  }
 })

module.exports = mongoose.model("Order", orderSchema);