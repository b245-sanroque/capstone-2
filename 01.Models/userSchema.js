const mongoose = require("mongoose");
//require mongoose for creation of schema

// Create User Schema
 const userSchema = new mongoose.Schema({
 	username: {
 		type: String,
 		required: [true, "Email is required!"]
 	},
 	firstName: {
 		type: String,
 		required: [true, "First Name is required!"]
 	},
 	lastName: {
 		type: String,
 		required: [true, "Last Name is required!"]
 	},
 	email: {
 		type: String,
 		required: [true, "Email is required!"]
 	},
 	password: {
 		type: String,
 		required: [true, "Password is required!"]
 	},
 	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: Number,
		required: [true, "Mobile Number is required!"] 
	}
 });

// for model to be exportable
module.exports = mongoose.model("User", userSchema);	